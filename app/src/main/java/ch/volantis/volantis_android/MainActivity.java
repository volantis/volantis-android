package ch.volantis.volantis_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONObject;
import org.json.JSONException;

import ch.uepaa.p2pkit.ConnectionCallbacks;
import ch.uepaa.p2pkit.ConnectionResult;
import ch.uepaa.p2pkit.ConnectionResultHandling;
import ch.uepaa.p2pkit.KitClient;
import ch.uepaa.p2pkit.discovery.InfoTooLongException;
import ch.uepaa.p2pkit.discovery.P2pListener;
import ch.uepaa.p2pkit.discovery.Peer;

public class MainActivity extends AppCompatActivity {

    private static final String APP_KEY = "eyJzaWduYXR1cmUiOiJCMmRMQkVpMTQ5MHJnaW00OW9zR1BWM2VJOUY1SE5qL09heHVKTjg0WktFQ2dTTDN5S3pRZHhGZktBWG9rTUQ0STU2WmlxeGlZbC9ob1RmWVV2NEd2WFhLUEhxWFJIbkVwQjVkTWRTWVZNQzliak9BV2dHVnZ6RkNTdHVnTGpEN29PclFPR1Q3dUxMMXlwOWY1aGtMai9qQ2tSK1ZSQU9SMGhkanVuRy9FSFk9IiwiYXBwSWQiOjEyOTEsInZhbGlkVW50aWwiOjE2ODAwLCJhcHBVVVVJRCI6IkM0Qjg5RkE3LUMwN0UtNEU3Ni05ODhGLUJFQkQ1MjZGRDNBMiJ9";

    // Initialization (1/2) - Connect to the P2P Services
    private void enableKit() {

        final int statusCode = KitClient.isP2PServicesAvailable(this);
        if (statusCode == ConnectionResult.SUCCESS) {
            KitClient client = KitClient.getInstance(this);
            client.registerConnectionCallbacks(mConnectionCallbacks);

            if (client.isConnected()) {
                logToView("Client already connected");
            } else {
                logToView("Connecting P2PKit client");
                client.connect(APP_KEY);
            }
        } else {
            logToView("Cannot start P2PKit, status code: " + statusCode);
            ConnectionResultHandling.showAlertDialogForConnectionError(this, statusCode);
        }
    }

    // Initialization (2/2) - Handle the connection status callbacks with the P2P Services
    private final ConnectionCallbacks mConnectionCallbacks = new ConnectionCallbacks() {

        @Override
        public void onConnected() {
            logToView("Successfully connected to P2P Services, with node id: " + KitClient.getInstance(MainActivity.this).getNodeId().toString());

            mP2pSwitch.setEnabled(true);

            if (mShouldStartServices) {
                mShouldStartServices = false;

                startP2pDiscovery();
            }
        }

        @Override
        public void onConnectionSuspended() {
            logToView("Connection to P2P Services suspended");

            mP2pSwitch.setEnabled(false);
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            logToView("Connection to P2P Services failed with status: " + connectionResult.getStatusCode());
            ConnectionResultHandling.showAlertDialogForConnectionError(MainActivity.this, connectionResult.getStatusCode());
        }
    };

    private void disableKit() {
        KitClient.getInstance(this).disconnect();
    }

    private void startP2pDiscovery() {
        try {
            KitClient.getInstance(this).getDiscoveryServices().setP2pDiscoveryInfo("Valora".getBytes());
        } catch (InfoTooLongException e) {
            logToView("P2pListener | The discovery info is too long");
        }
        KitClient.getInstance(this).getDiscoveryServices().addListener(mP2pDiscoveryListener);
    }

    // Listener of P2P discovery events
    private final P2pListener mP2pDiscoveryListener = new P2pListener() {

        @Override
        public void onStateChanged(final int state) {
            logToView("P2pListener | State changed: " + state);
        }

        @Override
        public void onPeerDiscovered(final Peer peer) {
            // Check the discovery information to guarantee that we only allow Valora customers to connect among each other
            String discoveryInfo = new String(peer.getDiscoveryInfo());

            if (discoveryInfo != null && discoveryInfo.equals("Valora")) {
                // We found a valid peer we want to challenge
                logToView("P2pListener | Peer discovered: " + peer.getNodeId() + " with discovery info: " + discoveryInfo);

                // Get NodeIDs of both participating peers
                String myId = KitClient.getInstance(MainActivity.this).getNodeId().toString();
                String discoveryId = peer.getNodeId().toString();

                // TODO: potentially confirm before we start the game

                // Change to the WebView
                setContentView(R.layout.activity_webclient);
                WebView myWebView = (WebView) findViewById(R.id.webview);
                myWebView.getSettings().setJavaScriptEnabled(true);
                // Scale webpage to device screen
                myWebView.getSettings().setLoadWithOverviewMode(true);
                myWebView.getSettings().setUseWideViewPort(true);
                myWebView.setWebViewClient(new WebViewClient());

                // Pack UUIDs in a JSON Object
                JSONObject object = new JSONObject();
                try {
                    object.put("id", myId);
                    object.put("discovery_id", discoveryId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Pass the UUIDs to the web app
                String json = object.toString();
                myWebView.loadUrl("http://volantis.bimmler.ch:5000/discovery('"+json+"')");

            } else {
                // We discovered an invalid peer, i.e. a peer not using the Valora app
                logToView("P2pListener | Peer discovered: " + peer.getNodeId() + " with invalid discovery info");
            }
        }

        @Override
        public void onPeerLost(final Peer peer) {
            logToView("P2pListener | Peer lost: " + peer.getNodeId());
        }

        @Override
        public void onPeerUpdatedDiscoveryInfo(Peer peer) {
            logToView("P2pListener | Peer updated");
        }
    };

    private void stopP2pDiscovery() {
        KitClient.getInstance(this).getDiscoveryServices().removeListener(mP2pDiscoveryListener);
        logToView("P2pListener removed");
    }

    private boolean mShouldStartServices;

    private TextView mLogView;
    private Switch mP2pSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupUI();

        mShouldStartServices = true;
        enableKit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupUI() {
        mLogView = (TextView) findViewById(R.id.textView);

        findViewById(R.id.clearTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLogs();
            }
        });

        Switch kitSwitch = (Switch) findViewById(R.id.kitSwitch);
        kitSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    enableKit();
                } else {
                    mP2pSwitch.setChecked(false);
                    disableKit();
                }
            }
        });

        mP2pSwitch = (Switch) findViewById(R.id.p2pSwitch);
        mP2pSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startP2pDiscovery();
                } else {
                    stopP2pDiscovery();
                }
            }
        });
    }

    private void logToView(String message) {
        CharSequence currentTime = DateFormat.format("hh:mm:ss - ", System.currentTimeMillis());
        mLogView.setText(currentTime + message + "\n" + mLogView.getText());
    }

    private void clearLogs() {
        mLogView.setText("");
    }
}